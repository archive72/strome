package flags

type NoFileSpecifiedError struct{}

func (e NoFileSpecifiedError) Error() string {
	return "no file specified"
}
