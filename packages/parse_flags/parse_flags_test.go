package flags_test

//nolint
import (
	flags "doppeldenken/strome/packages/parse_flags"
	"errors"
	"flag"
	"os"
	"testing"
)

//nolint:paralleltest
func TestRun(t *testing.T) {
	testCases := getRunTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.testName, func(t *testing.T) {
			flagSetup(testCase.flags)

			r, err := flags.Get()

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if r != testCase.expectedFlags {
				t.Errorf(
					"got flags '%v', expected flags '%v'",
					r,
					testCase.expectedFlags,
				)
			}
		})
	}
}

type parseFlagsTestCase struct {
	testName      string
	flags         []string
	expectedFlags flags.Flags
	expectedErr   error
}

//nolint
func getRunTestCases() []parseFlagsTestCase {
	return []parseFlagsTestCase{
		{
			testName: "no passed flags - error expected",
			flags:    []string{},
			expectedFlags: flags.Flags{
				Filepath: "",
				LogLevel: 1,
				Version:  false,
			},
			expectedErr: flags.NoFileSpecifiedError{},
		},
		{
			testName: "filepath long flag",
			flags: []string{
				"--filepath",
				"movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "movie.torrent", Version: false, LogLevel: 1},
			expectedErr:   nil,
		},
		{
			testName: "filepath short flag",
			flags: []string{
				"-f",
				"movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "movie.torrent", Version: false, LogLevel: 1},
			expectedErr:   nil,
		},
		{
			testName: "filepath long and short flag (assumes value of the long flag)",
			flags: []string{
				"-f",
				"short.movie.torrent",
				"--filepath",
				"long.movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "long.movie.torrent", Version: false, LogLevel: 1},
			expectedErr:   nil,
		},
		{
			testName: "full path",
			flags: []string{
				"-f",
				"/tmp/movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "/tmp/movie.torrent", Version: false, LogLevel: 1},
			expectedErr:   nil,
		},
		{
			testName: "version long flag",
			flags: []string{
				"--version",
			},
			expectedFlags: flags.Flags{Filepath: "", Version: true, LogLevel: 1},
			expectedErr:   nil,
		},
		{
			testName: "version short flag",
			flags: []string{
				"-v",
			},
			expectedFlags: flags.Flags{Filepath: "", Version: true, LogLevel: 1},
			expectedErr:   nil,
		},
		{
			testName: "log level long flag",
			flags: []string{
				"--loglevel",
				"3",
				"--filepath",
				"long.movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "long.movie.torrent", Version: false, LogLevel: 3},
			expectedErr:   nil,
		},
		{
			testName: "log level short flag",
			flags: []string{
				"-l",
				"2",
				"--filepath",
				"long.movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "long.movie.torrent", Version: false, LogLevel: 2},
			expectedErr:   nil,
		},
		{
			testName: "log level wrong level",
			flags: []string{
				"-l",
				"10",
				"--filepath",
				"long.movie.torrent",
			},
			expectedFlags: flags.Flags{Filepath: "long.movie.torrent", Version: false, LogLevel: 1},
			expectedErr:   nil,
		},
	}
}

func flagSetup(flags []string) {
	resetFlags()

	os.Args = append(
		[]string{os.Args[0]},
		flags...,
	)
}

func resetFlags() {
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
}
