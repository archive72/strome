package utils

import (
	"os"
	"strings"
)

func (fileAdapter FileAdapter) Name(file *os.File, fullName bool) (name string, err error) {
	name = file.Name()

	if fullName {
		return name, nil
	}

	paths := strings.Split(name, "/")

	numberOfPaths := len(paths)

	if numberOfPaths == 0 {
		return "", BadFilenameError{
			Filename: name,
		}
	}

	return paths[numberOfPaths-1], nil
}
