package utils

import "encoding/json"

type IJsonAdapter interface {
	Marshal(v interface{}) (data []byte, err error)
	MarshalIndent(v interface{}, prefix string, indent string) ([]byte, error)
	Unmarshal(data []byte, v interface{}) error
}

type JsonAdapter struct{}

func (jsonAdapter JsonAdapter) Marshal(v interface{}) (data []byte, err error) {
	return json.Marshal(v)
}

func (jsonAdapter JsonAdapter) MarshalIndent(v interface{}, prefix string, indent string) ([]byte, error) {
	return json.MarshalIndent(v, "", "  ")
}

func (jsonAdapter JsonAdapter) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}
