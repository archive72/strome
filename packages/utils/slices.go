package utils

func CompareStringSlices(slice1, slice2 []string) bool {
	if len(slice1) != len(slice2) {
		return false
	}

	for i := 0; i < len(slice1); i++ {
		if slice1[i] != slice2[i] {
			return false
		}
	}

	return true
}

func CompareByteSlice(slice1, slice2 []byte) bool {
	if len(slice1) != len(slice2) {
		return false
	}

	for i := 0; i < len(slice1); i++ {
		if slice1[i] != slice2[i] {
			return false
		}
	}

	return true
}

func CompareBoolSlice(slice1, slice2 []bool) bool {
	if len(slice1) != len(slice2) {
		return false
	}

	for i := 0; i < len(slice1); i++ {
		if slice1[i] != slice2[i] {
			return false
		}
	}

	return true
}

func CheckIfEmptyByteSlice(slice []byte) (isEmpty bool, length int) {
	length = len(slice)

	if length == 0 {
		return true, length
	}

	for _, value := range slice {
		if value != 0 {
			return false, length
		}
	}

	return true, length
}
