package utils

import (
	"io"
	"os"
)

func (fileAdapter FileAdapter) Write(
	file *os.File,
	data []byte,
	offset int,
) (
	n int,
	err error,
) {
	if offset < 0 {
		end, err := file.Seek(0, io.SeekEnd)
		if err != nil {
			return 0, err
		}

		return file.WriteAt(data, end)
	} else {
		return file.WriteAt(data, int64(offset))
	}
}
