package utils

func Compare8ByteArray(array1, array2 [8]byte) bool {
	for i := 0; i < len(array1); i++ {
		if array1[i] != array2[i] {
			return false
		}
	}

	return true
}

func Compare20ByteArray(array1, array2 [20]byte) bool {
	for i := 0; i < len(array1); i++ {
		if array1[i] != array2[i] {
			return false
		}
	}

	return true
}
