package utils

import (
	"io/fs"
	"os"
)

type IDirAdapter interface {
	MkdirAll(path string, perm fs.FileMode) error
}

type DirAdapter struct{}

func (dirAdapter DirAdapter) MkdirAll(path string, perm fs.FileMode) error {
	return os.MkdirAll(path, perm)
}
