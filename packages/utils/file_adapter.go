package utils

import (
	"bytes"
	"os"
)

type IFileAdapter interface {
	Name(file *os.File, fullName bool) (name string, err error)
	Read(name string) (buffer bytes.Buffer, err error)
	Write(file *os.File, data []byte, offset int) (n int, err error)
}

type FileAdapter struct{}
