package files

//nolint
import (
	"doppeldenken/strome/packages/utils"

	"fmt"
)

func CreatePiecesFile(
	numberOfPieces int,
	torrentTmpDir string,
	fileAdapter utils.IFileAdapter,
	jsonAdapter utils.IJsonAdapter,
	osAdapter utils.IOsAdapter,
) error {
	piecesFilePath := fmt.Sprintf(
		"%v/%v",
		torrentTmpDir,
		PiecesFileName,
	)

	fileInfo, _ := osAdapter.Stat(piecesFilePath)
	if fileInfo != nil {
		return PiecesFileAlreadyExistsError{}
	}

	downloadedPieces := make(DownloadedPieces)

	for i := 0; i < numberOfPieces; i++ {
		downloadedPieces[i] = false
	}

	data, err := jsonAdapter.MarshalIndent(downloadedPieces, "", "  ")
	if err != nil {
		return err
	}

	file, err := osAdapter.Create(piecesFilePath, true)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fileAdapter.Write(file, data, -1)
	if err != nil {
		return err
	}

	return nil
}
