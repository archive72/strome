package files

const (
	StromeTmpDir       = "/tmp/.strome"
	PiecesFileName     = ".pieces.json"
	PiecesFileLockName = ".pieces.lock.json"
)
