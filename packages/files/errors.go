package files

type NoFileSpecifiedError struct{}

func (noFileSpecifiedError NoFileSpecifiedError) Error() string {
	return "no file(s) specified in torrent metadata file"
}

type PiecesFileAlreadyExistsError struct{}

func (piecesFileAlreadyExistsError PiecesFileAlreadyExistsError) Error() string {
	return "pieces files already exists"
}
