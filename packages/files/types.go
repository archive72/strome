package files

import "time"

type DownloadedPieces map[int]bool

func CompareDownloadedPieces(downloadedPieces1, downloadedPieces2 DownloadedPieces) bool {
	if len(downloadedPieces1) != len(downloadedPieces2) {
		return false
	}

	for key, _ := range downloadedPieces1 {
		if downloadedPieces1[key] != downloadedPieces2[key] {
			return false
		}
	}

	return true
}

type DownloadedPiecesLock struct {
	Time time.Time
}
