package files_test

//nolint
import (
	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/metadata"
	"io/fs"

	"errors"
	"testing"
)

func TestCreateTmpDirs(t *testing.T) {
	t.Parallel()

	testCases := getCreateTorrentTmpDirsTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			mkdirAllArgs := []mkdirAllArgs{}
			mockDirAdapter := mockDirAdapter{
				mkdirAllArgs: &mkdirAllArgs,
			}

			torrentTmpDirRoot, err := files.CreateTmpDirs(testCase.torrentMetadataInfo, mockDirAdapter)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if torrentTmpDirRoot != testCase.expectedTorrentTmpDirRoot {
				t.Errorf(
					"got torrentTmpDirRoot '%v', expected torrentTmpDirRoot '%v'",
					torrentTmpDirRoot,
					testCase.expectedTorrentTmpDirRoot,
				)
			}

			if !compareMkdirAllArgs(mkdirAllArgs, testCase.expectedMkdirAllArgs) {
				t.Errorf(
					"got mkdirAllArgs '%v', expected mkdirAllArgs '%v'",
					mkdirAllArgs,
					testCase.expectedMkdirAllArgs,
				)
			}
		})
	}
}

type mkdirAllArgs struct {
	path string
	perm fs.FileMode
}

type mockDirAdapter struct {
	mkdirAllArgs *[]mkdirAllArgs
}

func (dirAdapter mockDirAdapter) MkdirAll(path string, perm fs.FileMode) error {
	*dirAdapter.mkdirAllArgs = append(
		*dirAdapter.mkdirAllArgs,
		mkdirAllArgs{
			path: path,
			perm: perm,
		},
	)

	return nil
}

func compareMkdirAllArgs(mkdirAllArgs1, mkdirAllArgs2 []mkdirAllArgs) bool {
	if len(mkdirAllArgs1) != len(mkdirAllArgs2) {
		return false
	}

	for i := 0; i < len(mkdirAllArgs1); i++ {
		if mkdirAllArgs1[i].path != mkdirAllArgs2[i].path {
			return false
		}

		if mkdirAllArgs1[i].perm != mkdirAllArgs2[i].perm {
			return false
		}
	}

	return true
}

type createTorrentTmpDirsTestCase struct {
	name                      string
	torrentMetadataInfo       metadata.TorrentMetadataInfo
	expectedErr               error
	expectedTorrentTmpDirRoot string
	expectedMkdirAllArgs      []mkdirAllArgs
}

//nolint:funlen
func getCreateTorrentTmpDirsTestCases() []createTorrentTmpDirsTestCase {
	return []createTorrentTmpDirsTestCase{
		{
			name: "successfully create tmp files - one file torrent",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files:       []metadata.TorrentMetadataInfoFile{},
				Length:      1024,
				Name:        "a_torrent_file.txt",
				PieceLength: 0,
				Pieces:      "",
			},
			expectedErr:               nil,
			expectedTorrentTmpDirRoot: "/tmp/.strome/5ed807c61901fb571644909f8911ca835042b4f1",
			expectedMkdirAllArgs: []mkdirAllArgs{
				{
					path: "/tmp/.strome/5ed807c61901fb571644909f8911ca835042b4f1/.pieces",
					perm: 2147483648,
				},
			},
		},
		{
			name: "successfully create tmp files - multiple files torrent",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files: []metadata.TorrentMetadataInfoFile{
					{
						Length: 1024,
						Path:   []string{"a_file.txt"},
					},
					{
						Length: 1024,
						Path:   []string{"a_dir", "a_file_in_a_dir.txt"},
					},
				},
				Length:      0,
				Name:        "",
				PieceLength: 0,
				Pieces:      "",
			},
			expectedErr:               nil,
			expectedTorrentTmpDirRoot: "/tmp/.strome/3278abb682e004ca377339d86ee8cf57fb8dc65c",
			expectedMkdirAllArgs: []mkdirAllArgs{
				{
					path: "/tmp/.strome/3278abb682e004ca377339d86ee8cf57fb8dc65c/.pieces",
					perm: 2147483648,
				},
				{
					path: "/tmp/.strome/3278abb682e004ca377339d86ee8cf57fb8dc65c/a_dir",
					perm: 2147483648,
				},
			},
		},
		{
			name: "create tmp files fail - no files or name specified",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files:       []metadata.TorrentMetadataInfoFile{},
				Length:      1024,
				Name:        "",
				PieceLength: 0,
				Pieces:      "",
			},
			expectedErr:               files.NoFileSpecifiedError{},
			expectedTorrentTmpDirRoot: "",
			expectedMkdirAllArgs:      []mkdirAllArgs{},
		},
	}
}
