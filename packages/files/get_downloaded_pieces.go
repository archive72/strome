package files

//nolint
import (
	"doppeldenken/strome/packages/utils"
	"fmt"
)

func getDownloadedPieces(torrentRootTmpDir string) (downloadedPieces DownloadedPieces, err error) {
	osAdapter := utils.OsAdapter{}

	data, err := osAdapter.Read(
		fmt.Sprintf(
			"%v/%v",
			torrentRootTmpDir,
			PiecesFileName,
		),
	)
	if err != nil {
		return DownloadedPieces{}, err
	}

	jsonAdapter := utils.JsonAdapter{}

	err = jsonAdapter.Unmarshal(data, &downloadedPieces)
	if err != nil {
		return DownloadedPieces{}, err
	}

	return downloadedPieces, nil
}
