package strome

//nolint
import (
	"doppeldenken/strome/packages/observability"
	flags "doppeldenken/strome/packages/parse_flags"

	"fmt"
	"os"
)

func getFlags() flags.Flags {
	flags, err := flags.Get()

	logger := observability.GetLogger(flags.LogLevel)

	checkError(err)

	//nolint
	if flags.Version {
		fmt.Println(version)

		os.Exit(0)
	}

	logger.Log(
		fmt.Sprintf(
			"cli flags retrieved successfully: %v",
			flags,
		),
		observability.DebugLogLevel,
	)

	return flags
}
