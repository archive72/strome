package strome

//nolint
import (
	"doppeldenken/strome/packages/files"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	"doppeldenken/strome/packages/utils"

	"fmt"
)

func createTmpDirs(torrentMetadata metadata.TorrentMetadata) string {
	dirAdapter := utils.DirAdapter{}
	torrentRootTmpDir, err := files.CreateTmpDirs(torrentMetadata.Info, dirAdapter)
	checkError(err)

	logger := observability.GetLogger(-1)

	logger.Log(
		fmt.Sprintf(
			"torrent root tmp dir: %v",
			torrentRootTmpDir,
		),
		observability.DebugLogLevel,
	)

	return torrentRootTmpDir
}
