package strome

//nolint
import (
	"doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/download_context"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	"doppeldenken/strome/packages/tracker"

	"fmt"
	"time"
)

func downloadCycle(
	downloadContext download_context.DownloadContext,
	maxConnectionsQueue chan bool,
	metadata metadata.TorrentMetadata,
	availablePeersQueue chan tracker.Peer,
	piecesToDownloadQueue chan int,
	downloadFileAdapter download.DownloadFileAdapter,
	getMorePeersChannel,
	completedPieceCountChannel chan bool,
) {
	logger := observability.GetLogger(-1)

	defer func() {
		logger.Log("adding connection to queue", observability.DebugLogLevel)

		maxConnectionsQueue <- true
	}()

	select {
	default:
		logger.Log("no more peers available; waiting for more", observability.DebugLogLevel)

		getMorePeersChannel <- true

		time.Sleep(waitingPeriod * time.Second)

		return

	case availablePeer := <-availablePeersQueue:
		for {
			handshake := client.Handshake{
				Pstr:     "BitTorrent protocol",
				InfoHash: downloadContext.InfoHash,
				PeerId:   [20]byte(tracker.GetPeerIdAsByteArray()),
			}

			peerClient, err := handshake.GetClient(
				availablePeer.Ip,
				availablePeer.Port,
				downloadContext.InfoHash,
			)
			if err != nil {
				logger.Log(
					fmt.Sprintf(
						"failed to handshake with client '%v': %v",
						availablePeer.Ip,
						err,
					),
					observability.DebugLogLevel,
				)

				return
			}

			peerClient.BlocksPerPiece = downloadContext.BlocksPerPiece
			peerClient.LastBlockSize = downloadContext.LastBlockSize
			peerClient.LastPieceSize = downloadContext.LastPieceSize
			peerClient.PieceLength = metadata.Info.PieceLength
			peerClient.Ip = availablePeer.Ip
			peerClient.Port = availablePeer.Port

			pieceNumber := -1

			select {
			case _pieceNumber := <-piecesToDownloadQueue:
				logger.Log(
					fmt.Sprintf(
						"assigned piece '%v' to client '%v'",
						_pieceNumber,
						peerClient.Ip,
					),
					observability.DebugLogLevel,
				)

				pieceNumber = _pieceNumber

			default:
				logger.Log("no more pieces left to download", observability.DebugLogLevel)

				return
			}

			peerClient.IsLastPiece = pieceNumber == len(downloadContext.PiecesHashes)-1

			downloaded := download.Run(
				peerClient,
				pieceNumber,
				downloadFileAdapter,
				downloadContext.PiecesHashes[pieceNumber],
			)

			if !downloaded {
				logger.Log(
					fmt.Sprintf(
						"client '%v' failed to download piece '%v'",
						peerClient.Ip,
						pieceNumber,
					),
					observability.DebugLogLevel,
				)

				err = downloadFileAdapter.DeletePieceFile(fmt.Sprintf("%v", pieceNumber))
				if err != nil {
					logger.Log(
						fmt.Sprintf(
							"error removing failed piece '%v': %v",
							pieceNumber,
							err,
						),
						observability.DebugLogLevel,
					)
				}

				logger.Log(
					fmt.Sprintf(
						"removed failed piece '%v'",
						pieceNumber,
					),
					observability.DebugLogLevel,
				)

				piecesToDownloadQueue <- pieceNumber

				logger.Log(
					fmt.Sprintf(
						"added piece '%v' back to queue",
						pieceNumber,
					),
					observability.DebugLogLevel,
				)

				return
			}

			completedPieceCountChannel <- true
		}
	}
}
