package strome

//nolint
import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/observability"

	"fmt"
)

func cleanIncompletePieces(downloadFileAdapter download.DownloadFileAdapter) {
	cleanedPieces, err := downloadFileAdapter.CleanIncompletePieces()
	checkError(err)

	logger := observability.GetLogger(-1)

	logger.Log(
		fmt.Sprintf(
			"finish cleaning incomplete pieces: %v",
			cleanedPieces,
		),
		observability.DebugLogLevel,
	)
}
