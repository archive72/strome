package strome

const (
	maxConnections         = 50
	maxNumberOfQueuedPeers = 50
	version                = "0.0.1"
	waitingPeriod          = 3
)
