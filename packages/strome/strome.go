package strome

//nolint
import (
	"doppeldenken/strome/packages/download_context"
	"doppeldenken/strome/packages/observability"
)

func Run() {
	flags := getFlags()

	torrentMetadata := getMetadata(flags)

	downloadContext := download_context.DownloadContext{}
	downloadContext.Set(torrentMetadata)

	downloadFileAdapter,
		piecesToDownload :=
		prepareEnvForDownload(downloadContext, torrentMetadata)

	if len(piecesToDownload) > 0 {
		go downloadPieces(
			downloadContext,
			downloadFileAdapter,
			piecesToDownload,
			torrentMetadata,
		)
	} else {
		observability.GetLogger(flags.LogLevel).Log("all pieces already downloaded", observability.DebugLogLevel)
	}

	makeFilesFromPieces(
		downloadContext,
		torrentMetadata,
		downloadFileAdapter.TorrentTmpRootDir,
	)
}
