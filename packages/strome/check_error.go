package strome

//nolint
import (
	"doppeldenken/strome/packages/observability"

	"os"
)

func checkError(err error) {
	logger := observability.GetLogger(-1)

	if err != nil {
		logger.Log(err.Error(), observability.DebugLogLevel)

		os.Exit(1)
	}
}
