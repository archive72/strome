package strome

//nolint
import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/download_context"
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"

	"fmt"
	"time"
)

func downloadPieces(
	downloadContext download_context.DownloadContext,
	downloadFileAdapter download.DownloadFileAdapter,
	piecesToDownload []int,
	torrentMetadata metadata.TorrentMetadata,
) {
	logger := observability.GetLogger(-1)

	availablePeersQueue,
		completedPieceCountChannel,
		getMorePeersChannel,
		maxConnectionsQueue,
		piecesToDownloadQueue :=
		initializeChannels(
			torrentMetadata,
			downloadContext.NumberOfPieces,
			piecesToDownload,
		)

	completedPieces := downloadContext.NumberOfPieces - len(piecesToDownload)

	go ongoingPeerRefresh(
		availablePeersQueue,
		getMorePeersChannel,
		torrentMetadata,
	)

	for {
		select {
		case <-maxConnectionsQueue:
			logger.Log("connection available!", observability.DebugLogLevel)

		case <-completedPieceCountChannel:
			completedPieces += 1

			logger.Log(
				fmt.Sprintf(
					"piece completed! count %v",
					completedPieces,
				),
				observability.DebugLogLevel,
			)

			if completedPieces == downloadContext.NumberOfPieces {
				logger.Log(
					"all pieces downloaded!",
					observability.DebugLogLevel,
				)

				return
			}

		default:
			logger.Log(
				fmt.Sprintf(
					"max connections reached; waiting for '%v'",
					waitingPeriod,
				),
				observability.DebugLogLevel,
			)

			time.Sleep(time.Duration(waitingPeriod) * time.Second)

			continue
		}

		go downloadCycle(
			downloadContext,
			maxConnectionsQueue,
			torrentMetadata,
			availablePeersQueue,
			piecesToDownloadQueue,
			downloadFileAdapter,
			getMorePeersChannel,
			completedPieceCountChannel,
		)
	}
}
