package strome

//nolint
import (
	"doppeldenken/strome/packages/observability"
	"fmt"

	"os"
)

func removeTmpFiles(torrentRootTmpDir string) {
	err := os.RemoveAll(torrentRootTmpDir)
	if err != nil {
		observability.GetLogger(-1).Log(
			fmt.Sprintf(
				"an error occurred while removing tmp resources: '%v'",
				err,
			),
			observability.DebugLogLevel,
		)
	}
}
