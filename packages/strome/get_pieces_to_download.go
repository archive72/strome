package strome

//nolint
import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/observability"

	"fmt"
)

func getPiecesToDownload(downloadFileAdapter download.DownloadFileAdapter) []int {
	piecesToDownload, err := downloadFileAdapter.GetPiecesToDownload()
	checkError(err)

	logger := observability.GetLogger(-1)

	logger.Log(
		fmt.Sprintf(
			"pieces to download: %v",
			piecesToDownload,
		),
		observability.DebugLogLevel,
	)

	return piecesToDownload
}
