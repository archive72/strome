package strome

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/observability"
	flags "doppeldenken/strome/packages/parse_flags"

	"fmt"
)

func getMetadata(flags flags.Flags) metadata.TorrentMetadata {
	torrentMetadata, err := metadata.Get(flags.Filepath)
	checkError(err)

	logger := observability.GetLogger(-1)

	logger.Log(
		fmt.Sprintf(
			"metadata retrieved successfully: %v",
			torrentMetadata.Info.Name,
		),
		observability.DebugLogLevel,
	)

	torrentMetadata.PrettyPrint()

	return torrentMetadata
}
