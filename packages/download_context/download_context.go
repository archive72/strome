package download_context

import "doppeldenken/strome/packages/metadata"

type IDownloadContext interface {
	Set(torrentMetadata metadata.TorrentMetadata) error
}

type DownloadContext struct {
	BlocksPerPiece  int
	InfoHash        metadata.Sha1Hash
	LastBlockSize   int
	LastPieceSize   int
	LastPieceNumber int
	NumberOfPieces  int
	PiecesHashes    []metadata.Sha1Hash
}
