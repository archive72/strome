package download_context

//nolint
import (
	"doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/metadata"

	"math"
)

func (downloadContext *DownloadContext) Set(torrentMetadata metadata.TorrentMetadata) error {
	downloadContext.BlocksPerPiece = int(math.Ceil(float64(torrentMetadata.Info.PieceLength) / float64(client.BlockSize)))

	downloadContext.LastBlockSize =
		torrentMetadata.Info.PieceLength - ((downloadContext.BlocksPerPiece - 1) * client.BlockSize)

	downloadContext.LastPieceSize = torrentMetadata.Info.GetLastPieceSize()

	infoHash, err := torrentMetadata.Info.GetTorrentSha1Hash()
	if err != nil {
		return err
	}

	downloadContext.InfoHash = infoHash

	downloadContext.PiecesHashes = torrentMetadata.Info.GetPiecesSha1Hashes()

	downloadContext.NumberOfPieces = len(downloadContext.PiecesHashes)

	downloadContext.LastPieceNumber = downloadContext.NumberOfPieces - 1

	return nil
}
