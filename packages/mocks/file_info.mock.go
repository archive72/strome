package mocks

import (
	"io/fs"
	"time"
)

type MockFileInfo struct {
	FileSize int64
}

func (fileInfo MockFileInfo) IsDir() bool {
	return false
}

func (fileInfo MockFileInfo) Name() string {
	return "test"
}

func (fileInfo MockFileInfo) Size() int64 {
	return fileInfo.FileSize
}

func (fileInfo MockFileInfo) Mode() fs.FileMode {
	return 2147483648
}

func (fileInfo MockFileInfo) ModTime() time.Time {
	return time.Time{}
}

func (fileInfo MockFileInfo) Sys() interface{} {
	return nil
}
