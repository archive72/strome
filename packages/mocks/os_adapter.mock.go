package mocks

import (
	"io/fs"
	"os"
)

type MockOsAdapter struct {
	ReadDirError      bool
	ReadDirFiles      []DirEntryInfo
	RemovingFileError bool
}

func (osAdapter MockOsAdapter) Create(name string, truncate bool) (*os.File, error) {
	return nil, nil
}

func (osAdapter MockOsAdapter) Executable() (string, error) {
	return "", nil
}

func (osAdapter MockOsAdapter) Read(name string) ([]byte, error) {
	return []byte{}, nil
}

func (osAdapter MockOsAdapter) ReadDir(name string) (entries []fs.DirEntry, err error) {
	if osAdapter.ReadDirError {
		return []fs.DirEntry{}, fs.ErrInvalid
	}

	for _, readDirFile := range osAdapter.ReadDirFiles {
		entries = append(entries, MockDirEntry{
			EntryName:     "test",
			EntryIsDir:    false,
			EntryFileMode: 777,
			EntryFileInfo: MockFileInfo{
				FileSize: readDirFile.FileSize,
			},
			ErrorGettingFileInfo: readDirFile.ErrorGettingFileInfo,
		})
	}

	return entries, nil
}

func (osAdapter MockOsAdapter) Remove(name string) error {
	if osAdapter.RemovingFileError {
		return os.ErrPermission
	}

	return nil
}

func (osAdapter MockOsAdapter) Stat(name string) (fs.FileInfo, error) {
	return nil, nil
}

type DirEntryInfo struct {
	FileSize             int64
	ErrorGettingFileInfo bool
}
