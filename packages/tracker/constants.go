package tracker

const compactResponse = "1"

const notCompactResponse = "0"

const stromePeerId = "strome12345678901234"

func GetPeerIdAsByteArray() (peerId [20]byte) {
	for i := 0; i < len(peerId); i++ {
		peerId[i] = byte(stromePeerId[i])
	}

	return peerId
}

const trackerConnectionTimeout = 15
