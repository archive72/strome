package tracker

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/utils"

	"net/http"
)

type HttpClientGet interface {
	Get(url string) (resp *http.Response, err error)
}

type PeerId [20]byte

type GetRequest struct {
	InfoHash   metadata.Sha1Hash
	Id         PeerId
	Ip         string
	Port       int
	Uploaded   int
	Downloaded int
	Left       int
	Compact    int
}

type CompactGetRequestResponse struct {
	Failure  string `bencode:"failure reason"`
	Interval int    `bencode:"interval"`
	Peers    string `bencode:"peers"`
}

type NotCompactGetRequestResponse struct {
	Failure  string `bencode:"failure reason"`
	Interval int    `bencode:"interval"`
	Peers    []Peer `bencode:"peers"`
}

type Peer struct {
	Id   PeerId `bencode:"peer id"`
	Ip   string `bencode:"ip"`
	Port string `bencode:"port"`
}

func CompareCompactResponses(response1, response2 CompactGetRequestResponse) bool {
	switch {
	case response1.Failure != response2.Failure:
		return false
	case response1.Interval != response2.Interval:
		return false
	case response1.Peers != response2.Peers:
		return false
	default:
		return true
	}
}

func CompareNotCompactResponses(response1, response2 NotCompactGetRequestResponse) bool {
	switch {
	case response1.Failure != response2.Failure:
		return false
	case response1.Interval != response2.Interval:
		return false
	case !ComparePeers(response1.Peers, response2.Peers):
		return false
	default:
		return true
	}
}

func ComparePeers(peers1, peers2 []Peer) bool {
	if len(peers1) != len(peers2) {
		return false
	}

	for i := 0; i < len(peers1); i++ {
		switch {
		case !utils.Compare20ByteArray(peers1[i].Id, peers2[i].Id):
			return false
		case peers1[i].Ip != peers2[i].Ip:
			return false
		case peers1[i].Port != peers2[i].Port:
			return false
		default:
			break
		}
	}

	return true
}
