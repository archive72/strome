package metadata_test

//nolint
import (
	"doppeldenken/strome/packages/metadata"
	"doppeldenken/strome/packages/utils"
	"errors"

	"testing"
)

func TestGetTorrentSha1Hash(t *testing.T) {
	t.Parallel()

	testCases := getGetTorrentSha1HashTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			hash, err := testCase.torrentMetadataInfo.GetTorrentSha1Hash()

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !utils.Compare20ByteArray(hash, testCase.expectedHash) {
				t.Errorf(
					"got hash '%v', expected hash '%v'",
					hash,
					testCase.expectedHash,
				)
			}
		})
	}
}

type getTorrentSha1HashTestCase struct {
	name                string
	torrentMetadataInfo metadata.TorrentMetadataInfo
	expectedErr         error
	expectedHash        metadata.Sha1Hash
}

func getGetTorrentSha1HashTestCases() []getTorrentSha1HashTestCase {
	return []getTorrentSha1HashTestCase{
		{
			name: "successfully gets torrent sha1 hash",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Length:      124234,
				Name:        "puppy.jpg",
				PieceLength: 16384,
				Pieces:      `qwertyuiopasdfghjklz`,
				Files:       []metadata.TorrentMetadataInfoFile{},
			},
			expectedErr: nil,
			expectedHash: metadata.Sha1Hash{
				66, 117, 1, 213, 227, 64, 149, 227, 66, 96, 139, 120, 68, 201, 24, 91, 217, 197, 247, 19,
			},
		},
	}
}
