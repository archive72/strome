package metadata

//nolint
import (
	"doppeldenken/strome/packages/utils"

	bencode "github.com/jackpal/bencode-go"
)

func Get(name string) (torrentMetadata TorrentMetadata, err error) {
	fileAdapter := utils.FileAdapter{}

	buffer, err := fileAdapter.Read(name)
	if err != nil {
		return TorrentMetadata{}, err
	}

	bencode.Unmarshal(&buffer, &torrentMetadata)

	return torrentMetadata, nil
}
