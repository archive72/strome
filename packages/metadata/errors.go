package metadata

import "fmt"

type HashesPiecesMismatchError struct {
	NumberOfHashes int
	NumberOfPieces int
}

func (hashesPiecesMismatchError HashesPiecesMismatchError) Error() string {
	return fmt.Sprintf(
		"mismatch between number of hashes and number of pieces (%v vs %v)",
		hashesPiecesMismatchError.NumberOfHashes,
		hashesPiecesMismatchError.NumberOfPieces,
	)
}
