package metadata

func (torrentMetadataInfo TorrentMetadataInfo) GetPiecesSha1Hashes() (hashes PiecesHashes) {
	start := 0
	end := 20
	hashSize := 20

	for i := start; i < len(torrentMetadataInfo.Pieces); i += hashSize {
		subString := torrentMetadataInfo.Pieces[start:end]

		var hash Sha1Hash
		for i := 0; i < len(hash); i++ {
			hash[i] = subString[i]
		}

		hashes = append(hashes, hash)

		start += hashSize
		end += hashSize
	}

	return hashes
}
