package metadata

type TorrentMetadataInfoFile struct {
	Length int      `bencode:"length"`
	Path   []string `bencode:"path"`
}
