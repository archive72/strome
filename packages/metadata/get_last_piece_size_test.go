package metadata_test

//nolint
import (
	"doppeldenken/strome/packages/metadata"

	"testing"
)

func TestGetLastPieceSize(t *testing.T) {
	// t.Parallel()

	testCases := getGetLastPieceSizeTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			// t.Parallel()

			lastPieceSize := testCase.torrentMetadataInfo.GetLastPieceSize()

			if lastPieceSize != testCase.expectedLastPieceSize {
				t.Errorf(
					"got last piece size '%v', expected last piece size '%v'",
					lastPieceSize,
					testCase.expectedLastPieceSize,
				)
			}
		})
	}
}

type getLastPieceSizeTestCase struct {
	name                  string
	torrentMetadataInfo   metadata.TorrentMetadataInfo
	expectedLastPieceSize int
}

func getGetLastPieceSizeTestCases() []getLastPieceSizeTestCase {
	return []getLastPieceSizeTestCase{
		{
			name: "successfully gets last piece size - single file - size is same as piece length",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files:       []metadata.TorrentMetadataInfoFile{},
				Length:      15,
				Name:        "test",
				PieceLength: 5,
				Pieces:      "123456789123456789||123456789123456789||123456789123456789||",
			},
			expectedLastPieceSize: 5,
		},
		{
			name: "successfully gets last piece size - single file - size is NOT the same as piece length",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files:       []metadata.TorrentMetadataInfoFile{},
				Length:      15,
				Name:        "test",
				PieceLength: 4,
				Pieces:      "123456789123456789||123456789123456789||123456789123456789||123456789123456789||",
			},
			expectedLastPieceSize: 3,
		},
		{
			name: "successfully gets last piece size - multiple files - size is same as piece length",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files: []metadata.TorrentMetadataInfoFile{
					{
						Path:   []string{"file1"},
						Length: 3,
					},
					{
						Path:   []string{"file2"},
						Length: 3,
					},
					{
						Path:   []string{"file3"},
						Length: 6,
					},
					{
						Path:   []string{"file4"},
						Length: 3,
					},
				},
				Length:      0,
				Name:        "test",
				PieceLength: 5,
				Pieces:      "123456789123456789||123456789123456789||123456789123456789||",
			},
			expectedLastPieceSize: 5,
		},
		{
			name: "successfully gets last piece size - multiple files - size is NOT the same as piece length",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Files: []metadata.TorrentMetadataInfoFile{
					{
						Path:   []string{"file1"},
						Length: 3,
					},
					{
						Path:   []string{"file2"},
						Length: 3,
					},
					{
						Path:   []string{"file3"},
						Length: 6,
					},
					{
						Path:   []string{"file4"},
						Length: 3,
					},
				},
				Length:      0,
				Name:        "test",
				PieceLength: 2,
				Pieces:      "123456789123456789||123456789123456789||123456789123456789||123456789123456789||123456789123456789||123456789123456789||123456789123456789||123456789123456789||", //nolint
			},
			expectedLastPieceSize: 1,
		},
	}
}
