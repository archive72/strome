package metadata

func (torrentMetadataInfo TorrentMetadataInfo) GetLastPieceSize() int {
	numberOfPieces := len(torrentMetadataInfo.GetPiecesSha1Hashes())

	totalLength := 0
	if len(torrentMetadataInfo.Files) == 0 {
		totalLength = torrentMetadataInfo.Length
	} else {
		for _, file := range torrentMetadataInfo.Files {
			totalLength += file.Length
		}
	}

	if isLastPieceShorter := totalLength%torrentMetadataInfo.PieceLength != 0; !isLastPieceShorter {
		return torrentMetadataInfo.PieceLength
	} else {
		return totalLength - (torrentMetadataInfo.PieceLength * (numberOfPieces - 1))
	}
}
