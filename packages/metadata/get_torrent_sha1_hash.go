package metadata

import (
	"bytes"
	"crypto/sha1"

	bencode "github.com/jackpal/bencode-go"
)

func (torrentMetadataInfo TorrentMetadataInfo) GetTorrentSha1Hash() (hash Sha1Hash, err error) {
	var buf bytes.Buffer

	if torrentMetadataInfo.Length != 0 {
		infoHashStruct := oneFileHash{
			Length:      torrentMetadataInfo.Length,
			Name:        torrentMetadataInfo.Name,
			Pieces:      torrentMetadataInfo.Pieces,
			PieceLength: torrentMetadataInfo.PieceLength,
		}

		err = bencode.Marshal(&buf, infoHashStruct)

		if err != nil {
			return hash, err
		}
	} else {
		infoHashStruct := multipleFilesHash{
			Files:       torrentMetadataInfo.Files,
			Pieces:      torrentMetadataInfo.Pieces,
			PieceLength: torrentMetadataInfo.PieceLength,
			Name:        torrentMetadataInfo.Name,
		}

		err = bencode.Marshal(&buf, infoHashStruct)

		if err != nil {
			return hash, err
		}
	}

	hash = sha1.Sum(buf.Bytes())

	return hash, nil
}
