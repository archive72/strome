package metadata

//nolint
import (
	"math"
)

func (torrentMetadataInfo TorrentMetadataInfo) CalculateFilePieceLimits() (filePieceLimits FilePieceLimits) {
	filePieceLimits = FilePieceLimits{}

	totalBytesRead := 0.00001
	startPiece := 0
	endPiece := 0
	startPieceOffset := 0

	for i, file := range torrentMetadataInfo.Files {
		totalBytesRead += float64(file.Length)

		filePieceLimit := FilePieceLimit{
			StartPiece:       startPiece,
			StartPieceOffset: startPieceOffset,
		}

		endPiece = int(math.Floor(totalBytesRead / float64(torrentMetadataInfo.PieceLength)))

		isEndOfPiece := totalBytesRead == (float64(endPiece)*float64(torrentMetadataInfo.PieceLength) + 0.00001)

		if isEndOfPiece {
			filePieceLimit.Pieces = createFilePieceMap(
				endPiece-1,
				torrentMetadataInfo.PieceLength,
				startPiece,
				startPieceOffset,
			)
		} else {
			filePieceLimit.Pieces = createFilePieceMap(
				endPiece,
				torrentMetadataInfo.PieceLength,
				startPiece,
				startPieceOffset,
			)
		}

		startPieceOffset = int(totalBytesRead) - endPiece*torrentMetadataInfo.PieceLength

		startPiece = endPiece

		isLastFile := (i + 1) == len(torrentMetadataInfo.Files)
		if isLastFile {
			filePieceLimit.EndPiece = int(
				math.Ceil(
					float64(torrentMetadataInfo.Length)/float64(torrentMetadataInfo.PieceLength),
				),
			) - 1
			filePieceLimit.EndPieceOffset = torrentMetadataInfo.PieceLength - 1
		} else {
			if isEndOfPiece {
				filePieceLimit.EndPiece = startPiece - 1
				filePieceLimit.EndPieceOffset = torrentMetadataInfo.PieceLength + 1
			} else {
				filePieceLimit.EndPiece = startPiece
				filePieceLimit.EndPieceOffset = startPieceOffset
			}
		}

		filePieceLimits[file.Path[len(file.Path)-1]] = filePieceLimit
	}

	return filePieceLimits
}

func createFilePieceMap(
	endPiece,
	pieceLength,
	startPiece,
	startPieceOffset int,
) map[PieceIndex]FileOffset {
	fileOffset := 0
	filePieceMap := make(map[PieceIndex]FileOffset)

	filePieceMap[startPiece] = fileOffset
	bytesFromFirstPiece := pieceLength - startPieceOffset
	fileOffset += bytesFromFirstPiece

	for i := startPiece + 1; i <= endPiece; i++ {
		filePieceMap[i] = fileOffset
		fileOffset += pieceLength
	}

	return filePieceMap
}
