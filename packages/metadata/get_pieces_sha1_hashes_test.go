package metadata_test

//nolint
import (
	"doppeldenken/strome/packages/metadata"

	"testing"
)

func TestGetPiecesSha1Hashes(t *testing.T) {
	t.Parallel()

	testCases := getGetPiecesSha1HashesTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			hashes := testCase.torrentMetadataInfo.GetPiecesSha1Hashes()

			if !metadata.ComparePiecesHashes(hashes, testCase.expectedHashes) {
				t.Errorf(
					"got hashes '%v', expected hashes '%v'",
					hashes,
					testCase.expectedHashes,
				)
			}
		})
	}
}

type getGetPiecesSha1HashesTestCase struct {
	name                string
	torrentMetadataInfo metadata.TorrentMetadataInfo
	expectedHashes      []metadata.Sha1Hash
}

func getGetPiecesSha1HashesTestCases() []getGetPiecesSha1HashesTestCase {
	return []getGetPiecesSha1HashesTestCase{
		{
			name: "successfully gets the expected hashes",
			torrentMetadataInfo: metadata.TorrentMetadataInfo{
				Length:      124234,
				Name:        "puppy.jpg",
				PieceLength: 16384,
				Pieces:      `qwertyuiopasdfghjklz`,
				Files:       []metadata.TorrentMetadataInfoFile{},
			},
			expectedHashes: []metadata.Sha1Hash{
				{113, 119, 101, 114, 116, 121, 117, 105, 111, 112, 97, 115, 100, 102, 103, 104, 106, 107, 108, 122},
			},
		},
	}
}
