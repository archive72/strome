package download_test

// //nolint
// import (
// 	"doppeldenken/strome/packages/client"
// 	"doppeldenken/strome/packages/download"
// 	"doppeldenken/strome/packages/files"
// 	"doppeldenken/strome/packages/mocks"
// 	"doppeldenken/strome/packages/utils"

// 	"errors"
// 	"io/fs"
// 	"os"
// 	"testing"
// )

// func TestDownloadFileAdapterGetPieceToDownload(t *testing.T) {
// 	t.Parallel()

// 	testCases := getDownloadFileAdapterGetPieceToDownloadTestCases()

// 	for _, testCase := range testCases {
// 		testCase := testCase

// 		t.Run(testCase.name, func(t *testing.T) {
// 			t.Parallel()

// 			downloadFileAdapter := download.DownloadFileAdapter{
// 				FileAdapter: utils.FileAdapter{},
// 				JsonAdapter: utils.JsonAdapter{},
// 				OsAdapter: mockGetPieceToDownloadOsAdapter{
// 					allPiecesBeingDownloadedError: testCase.allPiecesBeingDownloadedError,
// 				},
// 				TorrentTmpRootDir: "",
// 			}

// 			pieceToDownload, err := downloadFileAdapter.GetPieceToDownload(testCase.downloadedPieces, testCase.bitfield)

// 			if !errors.Is(err, testCase.expectedErr) {
// 				t.Errorf(
// 					"got error '%v', expected error '%v'",
// 					err,
// 					testCase.expectedErr,
// 				)
// 			}

// 			if pieceToDownload != testCase.expectedPieceNumber {
// 				t.Errorf(
// 					"got pieceToDownload '%v', expected pieceToDownload '%v'",
// 					pieceToDownload,
// 					testCase.expectedPieceNumber,
// 				)
// 			}
// 		})
// 	}
// }

// type mockGetPieceToDownloadOsAdapter struct {
// 	allPiecesBeingDownloadedError bool
// }

// func (fileAdapter mockGetPieceToDownloadOsAdapter) Create(name string) (*os.File, error) {
// 	return nil, nil
// }

// func (fileAdapter mockGetPieceToDownloadOsAdapter) Read(name string) ([]byte, error) {
// 	return []byte{}, nil
// }

// func (fileAdapter mockGetPieceToDownloadOsAdapter) ReadDir(name string) (entries []fs.DirEntry, err error) {
// 	return []fs.DirEntry{}, nil
// }

// func (fileAdapter mockGetPieceToDownloadOsAdapter) Remove(name string) error {
// 	return nil
// }

// func (fileAdapter mockGetPieceToDownloadOsAdapter) Stat(name string) (fs.FileInfo, error) {
// 	if fileAdapter.allPiecesBeingDownloadedError {
// 		return mocks.MockFileInfo{}, nil
// 	}

// 	return nil, nil
// }

// type downloadFileAdapterGetPieceToDownloadTestCase struct {
// 	name                          string
// 	allPiecesBeingDownloadedError bool
// 	downloadedPieces              files.DownloadedPieces
// 	bitfield                      client.Bitfield
// 	expectedErr                   error
// 	expectedPieceNumber           int
// }

// func getDownloadFileAdapterGetPieceToDownloadTestCases() []downloadFileAdapterGetPieceToDownloadTestCase {
// 	return []downloadFileAdapterGetPieceToDownloadTestCase{
// 		{
// 			name:                          "successfully gets piece to download",
// 			allPiecesBeingDownloadedError: false,
// 			downloadedPieces: files.DownloadedPieces{
// 				0: true,
// 				1: false,
// 				2: true,
// 				3: false,
// 			},
// 			bitfield: client.Bitfield{
// 				Pieces: []bool{
// 					true, true, true, true,
// 				},
// 			},
// 			expectedErr:         nil,
// 			expectedPieceNumber: 1,
// 		},
// 		{
// 			name:                          "fails to get piece to download - no available piece in bitfield",
// 			allPiecesBeingDownloadedError: false,
// 			downloadedPieces: files.DownloadedPieces{
// 				0: true,
// 				1: false,
// 				2: true,
// 				3: false,
// 			},
// 			bitfield: client.Bitfield{
// 				Pieces: []bool{
// 					false, false, true, false,
// 				},
// 			},
// 			expectedErr:         download.NoAvailablePieceError{},
// 			expectedPieceNumber: 0,
// 		},
// 		{
// 			name:                          "unsuccessfully gets piece to download - all pieces are already being downloaded",
// 			allPiecesBeingDownloadedError: true,
// 			downloadedPieces: files.DownloadedPieces{
// 				0: true,
// 				1: false,
// 				2: true,
// 				3: false,
// 			},
// 			bitfield: client.Bitfield{
// 				Pieces: []bool{
// 					true, true, true, true,
// 				},
// 			},
// 			expectedErr:         download.NoAvailablePieceError{},
// 			expectedPieceNumber: 0,
// 		},
// 	}
// }
