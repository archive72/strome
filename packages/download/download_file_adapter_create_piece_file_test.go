package download_test

//nolint
import (
	"doppeldenken/strome/packages/download"
	"doppeldenken/strome/packages/utils"

	"errors"
	"io/fs"
	"os"
	"testing"
)

func TestDownloadFileAdapterCreatePieceFile(t *testing.T) {
	t.Parallel()

	testCases := getDownloadFileAdapterCreatePieceFileTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			downloadFileAdapter := download.DownloadFileAdapter{
				OsAdapter:         testCase.mockOsAdapter,
				TorrentTmpRootDir: "/tmp/.strome",
			}

			_, err := downloadFileAdapter.CreatePieceFile(testCase.fileName)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}
		})
	}
}

type mockDirEntry struct {
	name     string
	isDir    bool
	fileMode fs.FileMode
}

func (dirEntry mockDirEntry) Name() string {
	return dirEntry.name
}

func (dirEntry mockDirEntry) IsDir() bool {
	return dirEntry.isDir
}

func (dirEntry mockDirEntry) Type() fs.FileMode {
	return dirEntry.fileMode
}

func (dirEntry mockDirEntry) Info() (fs.FileInfo, error) {
	return nil, nil
}

type mockCreatePieceFileOsAdapter struct {
	readDirErr bool
}

func (osAdapter mockCreatePieceFileOsAdapter) Create(name string, truncate bool) (*os.File, error) {
	return nil, nil
}

func (osAdapter mockCreatePieceFileOsAdapter) Executable() (string, error) {
	return os.Executable()
}

func (osAdapter mockCreatePieceFileOsAdapter) Read(name string) ([]byte, error) {
	return []byte{}, nil
}

func (osAdapter mockCreatePieceFileOsAdapter) ReadDir(name string) (entries []fs.DirEntry, err error) {
	if osAdapter.readDirErr {
		return []fs.DirEntry{}, os.ErrNotExist
	}

	return []fs.DirEntry{
		mockDirEntry{
			name: "test",
		},
	}, nil
}

func (osAdapter mockCreatePieceFileOsAdapter) Remove(name string) error {
	return nil
}

func (osAdapter mockCreatePieceFileOsAdapter) Stat(name string) (fs.FileInfo, error) {
	return nil, nil
}

type downloadFileAdapterCreatePieceFileTestCase struct {
	name          string
	fileName      string
	mockOsAdapter utils.IOsAdapter
	expectedErr   error
}

func getDownloadFileAdapterCreatePieceFileTestCases() []downloadFileAdapterCreatePieceFileTestCase {
	return []downloadFileAdapterCreatePieceFileTestCase{
		{
			name:     "successfully creates a piece file",
			fileName: "idontexist",
			mockOsAdapter: mockCreatePieceFileOsAdapter{
				readDirErr: false,
			},
			expectedErr: nil,
		},
		{
			name:     "unsuccessfully creates a piece file - piece file already exists",
			fileName: "test",
			mockOsAdapter: mockCreatePieceFileOsAdapter{
				readDirErr: false,
			},
			expectedErr: download.PieceFileAlreadyExistsError{
				Name: "test",
			},
		},
		{
			name:     "unsuccessfully creates a piece file - err reading dir",
			fileName: "test",
			mockOsAdapter: mockCreatePieceFileOsAdapter{
				readDirErr: true,
			},
			expectedErr: os.ErrNotExist,
		},
	}
}
