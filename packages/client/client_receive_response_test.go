package client_test

//nolint
import (
	"bytes"
	"net"

	"doppeldenken/strome/packages/client"
	"doppeldenken/strome/packages/mocks"

	"errors"
	"testing"
)

func TestReceiveResponse(t *testing.T) {
	t.Parallel()

	testCases := getReceiveResponseTestCases()

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			numberOfReads := 0

			mockConn := getReceiveResponseMockConn(testCase)
			mockConn.NumberOfReads = &numberOfReads

			client := client.Client{
				Conn: mockConn,
			}

			response, err := client.ReceiveResponse(testCase.expectedResponseCode)

			if !errors.Is(err, testCase.expectedErr) {
				t.Errorf(
					"got error '%v', expected error '%v'",
					err,
					testCase.expectedErr,
				)
			}

			if !bytes.Equal(response, testCase.expectedResponse) {
				t.Errorf(
					"got response '%v', expected response '%v'",
					response,
					testCase.expectedResponse,
				)
			}
		})
	}
}

type receiveResponseTestCase struct {
	name                         string
	firstReadError               bool
	secondReadError              bool
	onlyZeroBytesInResponseError bool
	expectedErr                  error
	expectedResponseCode         int
	expectedResponse             []byte
}

func getReceiveResponseMockConn(testCase receiveResponseTestCase) mocks.MockConn {
	mockConn := mocks.MockConn{
		NumberOfReads: nil,
		Testing:       "ReceiveResponse",
	}

	mockConn.FirstReadError = testCase.firstReadError
	mockConn.SecondReadError = testCase.secondReadError
	mockConn.OnlyZeroBytesError = testCase.onlyZeroBytesInResponseError

	return mockConn
}

func getReceiveResponseTestCases() []receiveResponseTestCase {
	return []receiveResponseTestCase{
		{
			name:                         "successful receive response",
			firstReadError:               false,
			secondReadError:              false,
			onlyZeroBytesInResponseError: false,
			expectedErr:                  nil,
			expectedResponseCode:         client.UnchokeCode,
			expectedResponse:             []byte{1, 2, 3, 4, 1, 2, 3, 4},
		},
		{
			name:                         "unsuccessful receive response - empty first read",
			firstReadError:               true,
			secondReadError:              false,
			onlyZeroBytesInResponseError: false,
			expectedErr: client.EmptyResponseError{
				Client: "0.0.0.0",
			},
			expectedResponseCode: -1,
			expectedResponse:     []byte{},
		},
		{
			name:                         "unsuccessful receive response - empty second read",
			firstReadError:               false,
			secondReadError:              true,
			onlyZeroBytesInResponseError: false,
			expectedErr:                  net.ErrClosed,
			expectedResponseCode:         -1,
			expectedResponse:             []byte{},
		},
		{
			name:                         "unsuccessful receive response - only zero bytes in second read",
			firstReadError:               false,
			secondReadError:              false,
			onlyZeroBytesInResponseError: true,
			expectedErr: client.EmptyResponseError{
				Client: "0.0.0.0",
			},
			expectedResponseCode: 0,
			expectedResponse:     []byte{},
		},
		{
			name:                         "unsuccessful receive response - received response code different from expected response code",
			firstReadError:               false,
			secondReadError:              false,
			onlyZeroBytesInResponseError: false,
			expectedErr: client.UnexpectedResponseCodeError{
				Client:   "0.0.0.0",
				Got:      1,
				Expected: 10,
			},
			expectedResponseCode: 10,
			expectedResponse:     []byte{},
		},
	}
}
