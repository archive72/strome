package client

//nolint
import (
	"doppeldenken/strome/packages/utils"

	"io"
)

type IHandshake interface {
	Deserialize(buffer []byte) error
	GetClient(ip, port string, infoHash [20]byte) (client Client, err error)
	Serialize() (buffer []byte, err error)
}

type Handshake struct {
	Pstr          string
	ReservedBytes [8]byte
	InfoHash      [20]byte
	PeerId        [20]byte
}

func (handshake *Handshake) GetClient(ip, port string, infoHash [20]byte) (client *Client, err error) {
	conn, err := utils.GetPeerTcpConnection(ip, port)
	if err != nil {
		return nil, utils.PeerConnectionError{
			Reason: err.Error(),
		}
	}

	client = &Client{}
	client.Conn = conn

	serializedHandshake, err := handshake.Serialize()
	if err != nil {
		return nil, err
	}

	_, err = client.Conn.Write(serializedHandshake)
	if err != nil {
		return nil, err
	}

	buffer := make([]byte, 68)

	_, err = client.Conn.Read(buffer)
	if err != nil && err != io.EOF {
		return nil, err
	}

	err = handshake.Deserialize(buffer)
	if err != nil {
		return nil, err
	}

	// if !utils.Compare20ByteArray(handshake.InfoHash, infoHash) {
	// 	return nil, DifferentInfoHashError{}
	// }

	return client, nil
}

func (handshake *Handshake) Deserialize(buffer []byte) error {
	pstrLen := int(buffer[0])

	incomingHandshakeSize := pstrLen + len(handshake.ReservedBytes) + len(handshake.InfoHash) + len(handshake.PeerId) + 1
	if incomingHandshakeSize != 68 && incomingHandshakeSize != 49 {
		return HandshakeSizeError{
			Size: incomingHandshakeSize,
		}
	}

	handshake.Pstr = string(buffer[1:20])

	copy(handshake.ReservedBytes[:], buffer[20:28])
	copy(handshake.InfoHash[:], buffer[28:48])

	if incomingHandshakeSize == 68 {
		copy(handshake.PeerId[:], buffer[48:68])
	}

	return nil
}

func (handshake *Handshake) Serialize() (buffer []byte, err error) {
	bufferSize := len(handshake.Pstr) + len(handshake.InfoHash) + len(handshake.PeerId) + HandshakeReservedBytesSize + 1

	if bufferSize != 68 {
		return buffer, HandshakeSizeError{
			Size: bufferSize,
		}
	}

	buffer = make([]byte, 1)

	buffer[0] = byte(len(handshake.Pstr))

	buffer = append(buffer, []byte(handshake.Pstr)...)

	buffer = append(buffer, handshake.ReservedBytes[:]...)

	buffer = append(buffer, handshake.InfoHash[:]...)

	buffer = append(buffer, handshake.PeerId[:]...)

	return buffer, nil
}
