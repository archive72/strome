package client

//nolint
import (
	"doppeldenken/strome/packages/utils"
	"math"

	"encoding/binary"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"time"
)

type IClient interface {
	IClientActions
	IClientUtils
}

type IClientActions interface {
	Close() error
	DownloadPiece(file *os.File, fileAdapter utils.IFileAdapter) (piece []byte, err error)
	GetPieceFromPeer(pieceNumber int) (piece []byte, err error)
	ReceiveResponse(expectedResponseCode int) (response []byte, err error)
	RequestPiece(pieceNumber int) error
	RequestBlock(pieceNumber, blockNumber int) error
	SendHave(pieceNumber int) error
	SendInterested() error
	Unchoke() error
}

type IClientUtils interface {
	GetClientTargetIp() string
	GetClientTargetPort() string
	RemoveDeadline() error
	SetDeadline(seconds time.Duration) error
}

type Client struct {
	BlocksPerPiece int
	Conn           net.Conn
	Ip             string
	IsLastPiece    bool
	LastBlockSize  int
	LastPieceSize  int
	PieceLength    int
	Port           string
}

func (client *Client) Close() error {
	return client.Conn.Close()
}

func (client *Client) DownloadPiece(file *os.File, fileAdapter utils.IFileAdapter) (piece []byte, err error) {
	defer client.RemoveDeadline()

	clientIp := client.GetClientTargetIp()

	name, err := fileAdapter.Name(file, false)
	if err != nil {
		return []byte{}, err
	}

	pieceNumber, err := strconv.Atoi(name)
	if err != nil {
		return []byte{}, err
	}

	client.SetDeadline(9)

	if err := client.SendInterested(); err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error sending interested message to client '%v' while downloading piece number '%v': %v",
				clientIp,
				pieceNumber,
				err,
			),
		)

		return []byte{}, err
	}

	fmt.Println(
		fmt.Sprintf(
			"successfully sent interested to client '%v",
			clientIp,
		),
	)

	response, err := client.ReceiveResponse(UnchokeCode)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error getting response from client '%v' to interested request: %v | %v",
				clientIp,
				response,
				err,
			),
		)

		return []byte{}, err
	}

	client.SetDeadline(30)

	piece, err = client.GetPieceFromPeer(pieceNumber)
	if err != nil {
		fmt.Println(
			fmt.Sprintf(
				"error getting piece '%v' from client '%v': '%v'",
				pieceNumber,
				clientIp,
				err,
			),
		)

		return []byte{}, err
	}

	fmt.Println(
		fmt.Sprintf(
			"successfully got piece '%v' from client '%v'",
			pieceNumber,
			clientIp,
		),
	)

	return piece, nil
}

func (client *Client) ReceiveResponse(expectedResponseCode int) (response []byte, err error) {
	lengthBuffer := make([]byte, 4)

	_, err = client.Conn.Read(lengthBuffer)
	if err != nil && err != io.EOF {
		return []byte{}, err
	}

	if isEmpty, _ := utils.CheckIfEmptyByteSlice(lengthBuffer); isEmpty {
		return []byte{}, EmptyResponseError{
			Client: client.GetClientTargetIp(),
		}
	}

	length := binary.BigEndian.Uint32(lengthBuffer)

	bytesRead := uint32(0)

	for bytesRead < length {
		tmpBuffer := make([]byte, 1024)

		n, err := client.Conn.Read(tmpBuffer)
		if err != nil && err != io.EOF {
			return []byte{}, err
		}

		response = append(response, tmpBuffer[:n]...)

		bytesRead += uint32(n)
	}

	if isEmpty, _ := utils.CheckIfEmptyByteSlice(response); isEmpty {
		return []byte{}, EmptyResponseError{
			Client: client.GetClientTargetIp(),
		}
	}

	if response[0] != byte(expectedResponseCode) {
		return []byte{}, UnexpectedResponseCodeError{
			Client:   client.GetClientTargetIp(),
			Expected: expectedResponseCode,
			Got:      int(response[0]),
		}
	}

	if response[0] == BitfieldCode {
		if utils.CompareByteSlice(response[(len(response)-5):], []byte{0, 0, 0, 1, 1}) {
			response = response[:(len(response) - 5)]
		}
	}

	return response, nil
}

func (client *Client) GetPieceFromPeer(pieceNumber int) (piece []byte, err error) {
	var blocks [][]byte

	clientIp := client.GetClientTargetIp()

	if client.IsLastPiece {
		blocksToDownload := int(math.Ceil(float64(client.LastPieceSize) / float64(BlockSize)))

		fmt.Println(
			fmt.Sprintf(
				"last piece has less blocks than the usual '%v': %v",
				client.BlocksPerPiece,
				blocksToDownload,
			),
		)

		client.BlocksPerPiece = blocksToDownload
		client.LastBlockSize = client.LastPieceSize - (BlockSize * (blocksToDownload - 1))
	}

	for blockNumber := 0; blockNumber < client.BlocksPerPiece; blockNumber++ {
		err = client.RequestBlock(pieceNumber, blockNumber)
		if err != nil {
			fmt.Println(
				fmt.Sprintf(
					"error requesting block '%v' of piece '%v' from client '%v': '%v'",
					blockNumber,
					pieceNumber,
					clientIp,
					err,
				),
			)

			return []byte{}, FailedGettingBlockError{
				Client: clientIp,
				Block:  blockNumber,
				Piece:  pieceNumber,
			}
		}

		response, err := client.ReceiveResponse(PieceCode)
		if err != nil {
			fmt.Println(
				fmt.Sprintf(
					"error receiving block '%v' of piece '%v' from client '%v': '%v'",
					blockNumber,
					pieceNumber,
					clientIp,
					err,
				),
			)

			return []byte{}, FailedGettingBlockError{
				Client: clientIp,
				Block:  blockNumber,
				Piece:  pieceNumber,
			}
		}

		block, err := parseBlock(response, pieceNumber, blockNumber)
		if err != nil {
			fmt.Println(
				fmt.Sprintf(
					"error parsing response while requesting block '%v' from piece '%v' from client '%v': '%v'",
					blockNumber,
					pieceNumber,
					clientIp,
					err,
				),
			)

			return []byte{}, FailedGettingBlockError{
				Client: clientIp,
				Block:  blockNumber,
				Piece:  pieceNumber,
			}
		}

		fmt.Println(
			fmt.Sprintf(
				"successfully received block '%v' of piece '%v' from client '%v'",
				blockNumber,
				pieceNumber,
				clientIp,
			),
		)

		blocks = append(blocks, block)
	}

	for _, block := range blocks {
		piece = append(piece, block...)
	}

	return piece, nil
}

func (client *Client) RequestBlock(pieceNumber, blockNumber int) error {
	payload := make([]byte, 12)

	binary.BigEndian.PutUint32(payload[0:4], uint32(pieceNumber))
	binary.BigEndian.PutUint32(payload[4:8], uint32(blockNumber*BlockSize))

	if blockNumber == client.BlocksPerPiece-1 {
		binary.BigEndian.PutUint32(payload[8:12], uint32(client.LastBlockSize))
	} else {
		binary.BigEndian.PutUint32(payload[8:12], uint32(BlockSize))
	}

	message := Message{
		Code:     RequestCode,
		ClientIp: client.GetClientTargetIp(),
		Payload:  payload,
	}

	_, err := client.Conn.Write(message.Serialize())

	return err
}

func (client *Client) RequestPiece(pieceNumber int) error {
	payload := make([]byte, 12)

	binary.BigEndian.PutUint32(payload[0:4], uint32(pieceNumber))
	binary.BigEndian.PutUint32(payload[4:8], uint32(0))
	binary.BigEndian.PutUint32(payload[8:12], uint32(BlockSize))

	message := Message{
		Code:     RequestCode,
		ClientIp: client.GetClientTargetIp(),
		Payload:  payload,
	}

	_, err := client.Conn.Write(message.Serialize())

	return err
}

func (client *Client) SendHave(pieceIndex int) error {
	payload := make([]byte, 4)

	binary.BigEndian.PutUint32(payload, uint32(pieceIndex))

	message := Message{
		Code:     HaveCode,
		ClientIp: client.GetClientTargetIp(),
		Payload:  payload,
	}

	_, err := client.Conn.Write(message.Serialize())

	return err
}

func (client *Client) SendInterested() error {
	message := Message{
		Code:     InterestedCode,
		ClientIp: client.GetClientTargetIp(),
		Payload:  []byte{},
	}

	_, err := client.Conn.Write(message.Serialize())

	return err
}

func (client *Client) Unchoke() error {
	message := Message{
		Code:     UnchokeCode,
		ClientIp: client.GetClientTargetIp(),
		Payload:  []byte{},
	}

	_, err := client.Conn.Write(message.Serialize())

	return err
}

func (client *Client) GetClientTargetIp() string {
	return client.Ip
}

func (client *Client) GetClientTargetPort() string {
	return client.Port
}

func (client *Client) RemoveDeadline() error {
	return client.Conn.SetDeadline(time.Time{})
}

func (client *Client) SetDeadline(seconds time.Duration) error {
	return client.Conn.SetDeadline(time.Now().Add(seconds * time.Second))
}

func parseBlock(
	response []byte,
	pieceNumber,
	blockNumber int,
) (block []byte, err error) {
	receivedPiece := int(binary.BigEndian.Uint32(response[1:5]))
	if receivedPiece != pieceNumber {
		return []byte{}, PeerSentWrongPieceError{
			Got:      receivedPiece,
			Expected: pieceNumber,
		}
	}

	receivedBlock := int(binary.BigEndian.Uint32(response[5:9]))
	if receivedBlock != blockNumber*BlockSize {
		return []byte{}, PeerSentWrongBlockError{
			Got:      receivedBlock,
			Expected: blockNumber,
		}
	}

	return response[9:], nil
}
