package client

import "doppeldenken/strome/packages/utils"

func CompareHandshakes(handshake1, handshake2 Handshake) bool {
	switch {
	case handshake1.Pstr != handshake2.Pstr:
		return false
	case !utils.Compare8ByteArray(handshake1.ReservedBytes, handshake2.ReservedBytes):
		return false
	case !utils.Compare20ByteArray(handshake1.InfoHash, handshake2.InfoHash):
		return false
	case !utils.Compare20ByteArray(handshake1.PeerId, handshake2.PeerId):
		return false
	default:
		return true
	}
}
